const express = require("express");
const app = express();
const port = 3000;

const studentList = [
  { id: 1, name: "hs1", age: 11 },
  { id: 2, name: "hs2", age: 12 },
  { id: 3, name: "hs3", age: 13 },
  { id: 4, name: "hs4", age: 14 },
  { id: 5, name: "hs5", age: 15 },
  { id: 6, name: "hs6", age: 16 },
];
// chuyển req, res về json
app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello World");
});
// lấy list học sinh
app.get("/students", (req, res) => {
  res.send("Lấy danh sách học sinh");
});

// lấy detail học sinh
app.get("/students/:id", (req, res) => {
  const params = req.params;
  const { id } = params;
  res.send(`Lấy chi tiết học sinh có id: ${id}`);
});

// thêm học sinh mới
app.post("/students", (req, res) => {
  const student = req.body;
  console.log({ student });
  res.send("Thêm học sinh");
});

// update học sinh mới
app.put("/students/:id", (req, res) => {
  const { id } = req.params;
  const student = req.body;
  console.log({ id }, { student });
  res.send(`Update học sinh có id: ${id}`);
});

// xoá học sinh
app.delete("/students/:id", (req, res) => {
  const { id } = req.params;
  console.log({ id });
  res.send(`Xoá học sinh có id: ${id}`);
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
